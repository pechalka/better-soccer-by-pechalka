﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.S))
		{
			GameObject go = (GameObject) Instantiate (Resources.Load ("platform_falling"));
			go.transform.position = transform.position + Vector3.right * 5f + Vector3.down * 3f;
			go.GetComponentInChildren<Rigidbody2D> ().AddForce (Vector3.right * 50f);
		}

		if (Input.GetKeyDown (KeyCode.W))
		{
			GameObject go = (GameObject) Instantiate (Resources.Load ("bullet"));

			var movingRight = GetComponent<Rigidbody2D> ().velocity.x >= 0f;
			var bonusVector = (movingRight) ? Vector3.right : Vector3.left;
			go.GetComponent<Bullet> ().BonusVector = bonusVector;
			go.transform.position = transform.position + bonusVector;
		}
	}
}
