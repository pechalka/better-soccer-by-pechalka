﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTwister : MonoBehaviour
{
	private int HP = 3;
	public Transform HealthBar;

	void OnCollisionEnter2D(Collision2D collision)
	{
		var controller = collision.gameObject.GetComponent<PlayerController> ();

		if (controller)
		{
			var pushVector = collision.transform.position - transform.position;
			pushVector = (pushVector.x <= 0) ? Vector3.left : Vector3.right;
			controller.GetComponent<Rigidbody2D> ().AddForce (pushVector * 1000f);
		}
			

		if (collision.gameObject.GetComponent<Bullet> ())
		{
			HP--;
			HealthBar.localScale = new Vector3 (0.05f * HP, 0.15f, 0.15f);
			if (HP == 0)
				Destroy (gameObject);
		}			
	}
}
