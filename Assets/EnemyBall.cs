﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBall : MonoBehaviour
{

	public int Size = 20;

	void OnCollisionEnter2D(Collision2D collision)
	{
		var controller = collision.gameObject.GetComponent<PlayerController> ();

		if (controller && (Size >= 2))
		{
			Size--;
			transform.localScale = Vector3.one * 0.05f * Size;
		}
	}
}
